<?php
    session_start();
    require_once "fungsi.php";
    if(!isset($_SESSION['login'])){
        header("Location:login.php");
    }
    if(isset($_POST['tambah'])){
        $nama=$_POST['nama'];
        $NIM=$_POST['NIM'];
        $jurusan=$_POST['jurusan'];
        InsertMaha($nama,$NIM,$jurusan);
        header("Location:index.php");
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Tambah</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
</head>
<body>
    <div class="container p-5">
        <h1>Tambah Mahasiswa</h1>
        <form style="width: 50%" action="" method="post">
            <label for="nama">Nama</label>
            <input required class="form-control" type="text" id="nama" name="nama"><br>
            <label for="NIM">NIM</label>
            <input required class="form-control" type="number" id="NIM" name="NIM"><br>
            <label for="jurusan">Jurusan</label>
            <input required class="form-control" type="text" id="jurusan" name="jurusan">
            <button class="btn btn-primary my-2" type="submit" name="tambah">Tambah</button>
        </form>
        <a href="index.php">Kembali</a>
    </div>
</body>
</html>
