<?php
    session_start();
    require_once "Funcusers.php";
    if(isset($_COOKIE['key']) && isset($_COOKIE['id'])){
        $id=$_COOKIE['id'];
        $username=$_COOKIE['key'];
        $user=GetUser($id);
        $key=$_COOKIE['key'];
        if($key!=hash("sha256",$user['username'])){
            session_destroy();
            header("Location:login.php");
        }else{
            $maha=GetUser($id);
            $_SESSION['login']=true;
        }
    }
    if(!isset($_SESSION['login'])){
        header("Location:login.php");
    }
     $title="PHPDasar";
     require_once "fungsi.php";
     $mahasiswa=GetMahasiswa();
     if(isset($_POST['search'])){
         $mahasiswa=Search($_POST['search']);
     }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?= $title ?></title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
</head>
<body>
    <div class="container p-4">
        <h1>Data Mahasiswa</h1>
        <?php if(isset($maha)) { ?>
            <h3 style="font-style: italic">Selamat Datang <?php echo $maha['username']?></h3>
        <?php }?>
        <form method="post" action="" style="width: 30%" class="d-flex">
            <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search" name="search" id="keyword">
            <button class="btn btn-outline-success" id="search" >Search</button>
        </form>
        <a  href="logout.php">Logout</a>
        <?php if(!$mahasiswa) { ?>
            <h1 class="my-4">Data Mahasiswa Tidak Ditemukan</h1>
        <?php } else{ ?>
           <div class="datamahasiswa">
               <table class="table" style="width: 70%">
                   <thead>
                   <tr>
                       <td>No</td>
                       <?php foreach ($mahasiswa[0] as $key=>$value) { ?>
                           <?php if($key!=="id") { ?>
                               <td><?= $key ?></td>
                           <?php }?>
                       <?php }?>
                       <td>Aksi</td>
                   </tr>
                   </thead>
                   <tbody>
                   <?php $no=1; ?>
                   <?php foreach ($mahasiswa as $value) { ?>
                       <tr>
                           <td><?php echo $no?></td>
                           <?php foreach ($value as $key=>$value1) { ?>
                               <?php if($key!=="id") { ?>
                                   <td><?php echo $value1 ?></td>
                               <?php }?>
                           <?php }?>
                           <td><a class="btn btn-outline-success" href="update.php?id=<?=$value['id'] ?>">update</a> <button class="btn btn-outline-danger" data-bs-toggle="modal" data-bs-target="#exampleModal<?= $value['id'] ?>">delete</button></td>
                           <?php $no++; ?>  
                           <div class="modal fade" id="exampleModal<?= $value['id'] ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                               <div class="modal-dialog">
                                   <div class="modal-content">
                                       <div class="modal-header">
                                           <h1 class="modal-title fs-5" id="exampleModalLabel">Konfirmasi</h1>
                                           <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                       </div>
                                       <div class="modal-body">
                                           <li>Nama : <?=$value['name'] ?></li>
                                           <li>Nim :<?=$value['NIM'] ?></li>
                                           <li>Jurusan : <?=$value['jurusan'] ?></li>
                                           <br>
                                           <h2>Ingin Tetap Menghapus???</h2>
                                       </div>
                                       <div class="modal-footer">
                                           <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                           <a type="button" class="btn btn-danger" href="delete.php?id=<?=$value['id'] ?>">delete</a>
                                       </div>
                                   </div>
                               </div>
                           </div>
                       </tr>
                   <?php }?>
                   </tbody>
               </table>
           </div>
        <?php }?>
        <a href="tambah.php" class="btn btn-outline-primary">Tambah</a>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>
    <script src="ajax.js"></script>
</body>
</html>
