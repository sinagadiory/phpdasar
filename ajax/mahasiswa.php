<?php
require_once "../fungsi.php";

$keyword=$_GET['keyword'];
$mahasiswa=Search($keyword);

?>
<?php if(!$mahasiswa) { ?>
    <h1>Data Mahasiswa Tidak Ditemukan</h1>
<?php } else { ?>
    <table class="table" style="width: 70%">
        <thead>
        <tr>
            <td>No</td>
            <?php foreach ($mahasiswa[0] as $key=>$value) { ?>
                <?php if($key!=="id") { ?>
                    <td><?= $key ?></td>
                <?php }?>
            <?php }?>
            <td>Aksi</td>
        </tr>
        </thead>
        <tbody>
        <?php $no=1; ?>
        <?php foreach ($mahasiswa as $value) { ?>
            <tr>
                <td><?php echo $no?></td>
                <?php foreach ($value as $key=>$value1) { ?>
                    <?php if($key!=="id") { ?>
                        <td><?php echo $value1 ?></td>
                    <?php }?>
                <?php }?>
                <td><a class="btn btn-outline-success" href="update.php?id=<?=$value['id'] ?>">update</a> <a class="btn btn-outline-danger" href="delete.php?id=<?=$value['id'] ?>">delete</a></td>
                <?php $no++; ?>
            </tr>
        <?php }?>
        </tbody>
    </table>
<?php }?>
