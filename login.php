<?php
    require_once "Funcusers.php";
    session_start();
    if(isset($_SESSION['login'])){
        header("Location:index.php");
    }
    if(isset($_POST['login'])){
        $user=GetUsername($_POST['username']);
        $password=$_POST['password'];
        if(!$user){
            $pesan="Username atau Password Salah";
        }else{
            $cekpassword=password_verify($password,$user['password']);
            if(!$cekpassword){
                $pesan="Username atau Password Salah";
            }else{
                $_SESSION['login']=true;
                setcookie("id",$user['id'],time()+3600);
                setcookie("key",hash("sha256",$user['username']),time()+3600);
                header("Location:index.php");
            }
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
</head>
<body>
    <div class="container p-5">
        <h1>Login Admin</h1>
        <?php if(isset($pesan)) { ?>
            <p style="color: red;font-style: italic"><?=$pesan ?></p>
        <?php } ?>
        <form style="width: 50%" action="" method="post">
            <label for="username">Username</label>
            <input required class="form-control" type="text" id="username" name="username"><br>
            <label for="password">Password</label>
            <input required class="form-control" type="password" id="password" name="password"><br>
            <button class="btn btn-primary my-2" type="login" name="login">Login</button>
            <a class="mx-5" href="register.php">Daftar Disini!</a>
        </form>
    </div>
</body>
</html>
