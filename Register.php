<?php
    session_start();
    require_once "Funcusers.php";
    if(isset($_SESSION['login'])){
        header("Location:index.php");
    }
    if(isset($_POST['register'])){
        $cari=GetUsername($_POST['username']);
        if($cari){
            $pesan="Username Sudah Terdaftar!";
        }else{
            $username=$_POST['username'];
            $password=$_POST['password'];
            if($password===$_POST['confpassword']){
                InsertUser($username,$password);
                header("Location:login.php");
            }else{
                $pesan="Password dan Confirm Password Tidak Sama!";
            }
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Register</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
</head>
<body>
    <div class="container p-5">
        <h1>Register</h1>
        <?php if(isset($pesan)) { ?>
            <p style="color: red;font-style: italic"><?=$pesan ?></p>
        <?php } ?>
        <form style="width: 50%" action="" method="post">
            <label for="username">Username</label>
            <input required class="form-control" type="text" id="username" name="username"><br>
            <label for="password">Password</label>
            <input required class="form-control" type="password" id="password" name="password"><br>
            <label for="confpassword">Konfirmasi Password</label>
            <input required class="form-control" type="password" id="confpassword" name="confpassword"><br>
            <button class="btn btn-success my-2" type="register" name="register">Register</button>
            <a class="mx-5" href="login.php">Login</a>
        </form>
    </div>
</body>
</html>
